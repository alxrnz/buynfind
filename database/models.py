from django.urls import reverse
from django_extensions.db.fields import AutoSlugField
from django.db.models import BooleanField
from django.db.models import DateTimeField
from django.db.models import EmailField
from django.db.models import IntegerField
from django.db.models import TextField
from django.db.models import URLField
from django.db.models import AutoField
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models
from django.db import models as models
from django_extensions.db import fields as extension_fields
from django.contrib.auth.models import AbstractUser


class Order(models.Model):
    # Fields
    amount = models.IntegerField()
    delivery_date = models.DateField()
    description = models.TextField(max_length=255)
    creation_date = models.DateTimeField()

    # Relationship Fields
    url_id = models.ForeignKey(
        'database.Url',
        on_delete=models.CASCADE, related_name="orders",
    )
    owner_id = models.ForeignKey(
        'database.User',
        on_delete=models.CASCADE, related_name="orders",
    )

    state_id = models.ForeignKey(
        'database.StatesOrder',
        on_delete=models.CASCADE, related_name="orders",
    )

    class Meta:
        ordering = ('-pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('database_order_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('database_order_update', args=(self.pk,))


class Article(models.Model):
    # Fields
    name = models.TextField(max_length=100)

    # Relationship Fields
    orders_article = models.ManyToManyField(
        'database.Order',
        related_name="articles",
    )
    category_id = models.ForeignKey(
        'database.Category',
        on_delete=models.CASCADE, related_name="articles",
    )

    class Meta:
        ordering = ('-pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('database_article_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('database_article_update', args=(self.pk,))


class Inventory(models.Model):
    # Fields
    amount = models.IntegerField()
    description = models.TextField(max_length=255)

    # Relationship Fields
    url_id = models.ForeignKey(
        'database.Url',
        on_delete=models.CASCADE, related_name="inventorys",
    )
    article_id = models.ForeignKey(
        'database.Article',
        on_delete=models.CASCADE, related_name="inventorys",
    )
    state_id = models.ForeignKey(
        'database.StatesInventory',
        on_delete=models.CASCADE, related_name="inventorys",
    )
    order_id = models.ForeignKey(
        'database.Order',
        on_delete=models.CASCADE, related_name="inventorys",
        blank=True,
        null=True
    )
    user = models.ForeignKey(
        'database.User',
        on_delete=models.CASCADE, related_name="users",
        blank=True,
        null=True
    )
    location = models.ForeignKey(
        'database.Location',
        on_delete=models.CASCADE, related_name="locations",
        blank=True,
        null=True
    )

    class Meta:
        ordering = ('-pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('database_inventory_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('database_inventory_update', args=(self.pk,))


class Category(models.Model):
    # Fields
    name = models.TextField(max_length=100)

    class Meta:
        ordering = ('-pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('database_category_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('database_category_update', args=(self.pk,))

    def __str__(self):
        return self.name


class Url(models.Model):
    # Fields
    shop = models.TextField(max_length=100)
    url = models.URLField()

    # Relationship Fields
    article_id = models.ForeignKey(
        'database.Article',
        on_delete=models.CASCADE, related_name="urls",
    )

    class Meta:
        ordering = ('-pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('database_url_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('database_url_update', args=(self.pk,))


class StatesOrder(models.Model):
    # Fields
    state = models.TextField(max_length=100)

    class Meta:
        ordering = ('-pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('database_states_orders_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('database_states_orders_update', args=(self.pk,))

    def __str__(self):
        return self.state


class StatesInventory(models.Model):
    # Fields
    state = models.TextField(max_length=100)

    class Meta:
        ordering = ('-pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('database_states_inventory_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('database_states_inventory_update', args=(self.pk,))

    def __str__(self):
        return self.state


class User(AbstractUser):
    # Fields
    is_secretary = models.BooleanField(default=False)
    tel = models.TextField(max_length=100)

    # Relationship Fields
    location_id = models.ForeignKey(
        'database.Location',
        on_delete=models.CASCADE, related_name="users",
        null=True
    )

    class Meta:
        ordering = ('-pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('database_users_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('database_users_update', args=(self.pk,))

    def __str__(self):
        return "{} ({})".format(self.username, self.email)


class Location(models.Model):
    # Fields
    postcode = models.TextField(max_length=100)
    city = models.TextField(max_length=100)
    street = models.TextField(max_length=100)
    streetnumber = models.TextField(max_length=100)
    room = models.TextField(max_length=100)

    class Meta:
        ordering = ('-pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('database_locations_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('database_locations_update', args=(self.pk,))

    def __str__(self):
        return "{} {} {}".format(self.street, self.streetnumber, self.room)
