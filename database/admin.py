from django.contrib import admin

# Register your models here.
from database.models import Order, Url, User, Location, StatesOrder, Article, Inventory, Category

admin.site.register(Order)
admin.site.register(Url)
admin.site.register(User)
admin.site.register(Location)
admin.site.register(StatesOrder)
admin.site.register(Article)
admin.site.register(Inventory)
admin.site.register(Category)