import datetime
from pprint import pprint

from django.db.models.query import EmptyQuerySet
from django.utils.timezone import utc
from urllib.parse import urlparse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import logout
from django.shortcuts import render, get_object_or_404, redirect
from .forms import OrderForm, StatesOrderForm, UserForm, LocationForm, CategoryForm, UrlForm, \
    ArticleForm, CategoryDropdownForm, StatesInventoryForm, InventoryForm, InventoryStateDropdownForm, \
    InventorySearchForm, OrderChangeStateForm, InventoryChangeStateForm, InventoryCategoryForm, UserSelectForm, \
    LocationSelectForm
from .models import Order, StatesOrder, User, Location, Category, Article, Url, StatesInventory, Inventory


# Create your views here.


# statesorder views

def statesorder_create_view(request):
    form = StatesOrderForm(request.POST or None)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/statesorders/')

    form = StatesOrderForm()
    context = {
        'form': form
    }
    return render(request, "./StatesOrder/StatesOrder_create.html", context)


def statesorder_list_view(request):
    queryset = StatesOrder.objects.all()
    context = {
        "object_list": queryset
    }
    return render(request, "./StatesOrder/StatesOrder_list.html", context)


def statesorder_detail_view(request, id):
    obj = get_object_or_404(StatesOrder, id=id)
    context = {
        "object": obj
    }
    return render(request, "./StatesOrder/StatesOrder_detail.html", context)


def statesorder_delete_view(request, id):
    obj = get_object_or_404(StatesOrder, id=id)
    if request.method == "POST":
        obj.delete()
        return redirect("../../")
    context = {
        "object": obj
    }
    return render(request, "./StatesOrder/StatesOrder_delete.html", context)


def statesorder_update_view(request, id=id):
    obj = get_object_or_404(StatesOrder, id=id)
    form = StatesOrderForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
    context = {
        "form": form
    }
    return render(request, "./StatesOrder/StatesOrder_create.html", context)


def statesinventory_create_view(request):
    form = StatesInventoryForm(request.POST or None)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/statesinventory")

    form = StatesInventoryForm()
    context = {
        'form': form
    }

    return render(request, "./StatesInventory/StatesInventory_create.html", context)


def statesinventory_list_view(request):
    queryset = StatesInventory.objects.all()
    context = {
        "object_list": queryset
    }
    return render(request, "./StatesInventory/StatesInventory_list.html", context)


def statesinventory_detail_view(request, id):
    obj = get_object_or_404(StatesInventory, id=id)
    context = {
        "object": obj
    }
    return render(request, "./StatesInventory/StatesInventory_detail.html", context)


def statesinventory_delete_view(request, id):
    obj = get_object_or_404(StatesInventory, id=id)
    if request.method == "POST":
        obj.delete()
        return redirect("../../")
    context = {
        "object": obj
    }
    return render(request, "./StatesInventory/StatesInventory_delete.html", context)


def statesinventory_update_view(request, id=id):
    obj = get_object_or_404(StatesInventory, id=id)
    form = StatesInventoryForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
    context = {
        "form": form
    }
    return render(request, "./StatesInventory/StatesInventory_create.html", context)


# Users View

def user_create_view(request):
    user_form = UserForm(request.POST or None)
    location_form = LocationForm(request.POST or None)

    if user_form.is_valid() and location_form.is_valid():
        location = Location.objects.create(postcode=location_form.cleaned_data['postcode'],
                                           city=location_form.cleaned_data['city'],
                                           street=location_form.cleaned_data['street'],
                                           streetnumber=location_form.cleaned_data['streetnumber'],
                                           room=location_form.cleaned_data['room'])
        location.save()
        user = User.objects.create(username=user_form.cleaned_data['username'],
                                   last_name=user_form.cleaned_data['last_name'],
                                   first_name=user_form.cleaned_data['first_name'],
                                   is_superuser=user_form.cleaned_data['is_superuser'],
                                   is_secretary=user_form.cleaned_data['is_secretary'],
                                   email=user_form.cleaned_data['email'],
                                   tel=user_form.cleaned_data['tel'],
                                   location_id=location)

        user.set_password(user_form.cleaned_data['password'])
        user.save()
        location_form = LocationForm()
        user_form = UserForm()
        return HttpResponseRedirect("../../auth/login/")
    else:
        location_form = LocationForm()
        user_form = UserForm()
    context = {
        'user_form': user_form,
        'location_form': location_form
    }
    return render(request, "./User/user_create.html", context)


def user_list_view(request):
    queryset = User.objects.all()
    context = {
        "object_list": queryset
    }
    return render(request, "./User/user_list.html", context)


def user_detail_view(request, id):
    obj = get_object_or_404(User, id=id)
    context = {
        "object": obj
    }
    return render(request, "./User/user_detail.html", context)


def user_delete_view(request, id):
    obj = get_object_or_404(User, id=id)
    if request.method == "POST":
        obj.delete()
        return redirect("../../")
    context = {
        "object": obj
    }
    return render(request, "./User/user_delete.html", context)


def user_update_view(request, id=id):
    obj = get_object_or_404(User, id=id)
    form = UserForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
    context = {
        "form": form
    }
    return render(request, "./User/user_create.html", context)


# Orders

def order_create_view(request):
    article_form = ArticleForm(request.POST or None)
    order_form = OrderForm(request.POST or None)
    url_form = UrlForm(request.POST or None)
    categoryDropdown_form = CategoryDropdownForm(request.POST or None)

    if article_form.is_valid() and order_form.is_valid() and url_form.is_valid() and categoryDropdown_form.is_valid():

        default_order_state = StatesOrder.objects.get(pk=1)

        if default_order_state is None:
            default_order_state = StatesOrder.objects.create(
                state="Ordered"
            )

            default_order_state.save()

        category = categoryDropdown_form.cleaned_data['category']

        article = Article.objects.create(name=article_form.cleaned_data['name'],
                                         category_id=category,
                                         )

        url = Url.objects.create(shop=urlparse(url_form.cleaned_data['url']).netloc,
                                 url=url_form.cleaned_data['url'],
                                 article_id=article,
                                 )

        order = Order.objects.create(amount=order_form.cleaned_data['amount'],
                                     delivery_date=order_form.cleaned_data['delivery_date'],
                                     description=order_form.cleaned_data['description'],
                                     creation_date=datetime.datetime.utcnow().replace(tzinfo=utc),
                                     url_id=url,
                                     owner_id=request.user,
                                     state_id=default_order_state
                                     )

        category.save()
        article.save()
        url.save()
        order.save()
        article.orders_article.add(order)

        return redirect("../")

    else:
        article_form = ArticleForm()
        url_form = UrlForm()
        order_form = OrderForm()
        categoryDropdown_form = CategoryDropdownForm()

    context = {
        'article_form': article_form,
        'order_form': order_form,
        'url_form': url_form,
        'categoryDropdown_form': categoryDropdown_form,
    }
    return render(request, "./Order/order_create.html", context)


def order_list_view(request):
    queryset = Order.objects.all()
    test = queryset.values_list()

    context = {
        "object_list": queryset
    }
    return render(request, "./Order/order_list.html", context)


def order_detail_view(request, id):
    obj = get_object_or_404(Order, id=id)
    context = {
        "object": obj
    }
    return render(request, "./Order/order_detail.html", context)


def order_delete_view(request, id):
    obj = get_object_or_404(Order, id=id)
    if request.method == "POST":
        obj.delete()
        return redirect("../../")
    context = {
        "object": obj
    }
    return render(request, "./Order/order_delete.html", context)


def order_update_view(request, id=id):
    obj = get_object_or_404(Order, id=id)
    form = OrderForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
    context = {
        "form": form
    }
    return render(request, "./Order/order_create.html", context)


def order_toinventory_view(request, id):
    obj = get_object_or_404(Order, id=id)
    if request.method == "POST":
        inventory = Inventory.objects.create(amount=obj.amount,
                                             description=obj.description,
                                             article_id=obj.url_id.article_id,
                                             order_id=obj,
                                             state_id=StatesInventory.objects.first(),
                                             url_id=obj.url_id
                                             )

        inventory.save()
        return redirect("../../../inventory/")
    context = {
        "object": obj
    }
    return render(request, "./Order/order_toInventory.html", context)


# ENDORDERS

# STARTINVENTORY

def inventory_create_view(request):
    article_form = ArticleForm(request.POST or None)
    inventory_form = InventoryForm(request.POST or None)
    url_form = UrlForm(request.POST or None)
    inventory_state_DropdownForm = InventoryStateDropdownForm(request.POST or None)
    categoryDropdown_form = CategoryDropdownForm(request.POST or None)
    user_select_form = UserSelectForm(request.POST or None)
    location_select_form = LocationSelectForm(request.POST or None)

    if article_form.is_valid() and inventory_form.is_valid() and url_form.is_valid() \
            and inventory_state_DropdownForm.is_valid() and categoryDropdown_form.is_valid() \
            and user_select_form.is_valid() and location_select_form.is_valid():

        category = categoryDropdown_form.cleaned_data['category']
        state = inventory_state_DropdownForm.cleaned_data['state']
        user = user_select_form.cleaned_data['user']
        location = location_select_form.cleaned_data['location']

        article = Article.objects.create(name=article_form.cleaned_data['name'],
                                         category_id=category)

        url = Url.objects.create(shop=urlparse(url_form.cleaned_data['url']).netloc,
                                 url=url_form.cleaned_data['url'],
                                 article_id=article)

        inventory = Inventory.objects.create(amount=inventory_form.cleaned_data['amount'],
                                             description=inventory_form.cleaned_data['description'],
                                             article_id=article,
                                             order_id=None,
                                             state_id=state,
                                             url_id=url,
                                             user=user,
                                             location=location
                                             )

        category.save()
        state.save()
        article.save()
        url.save()
        inventory.save()

        return redirect(inventory)

    context = {
        'article_form': article_form,
        'inventory_form': inventory_form,
        'url_form': url_form,
        'inventory_state_DropdownForm': inventory_state_DropdownForm,
        'categoryDropdown_form': categoryDropdown_form,
        'user_select_form': user_select_form,
        "location_select_form": location_select_form
    }
    return render(request, "./Inventory/inventory_create.html", context)


def inventory_list_view(request):
    order_by = request.GET.get('order_by')
    if order_by != None:
        queryset = Inventory.objects.all().order_by(order_by)
    else:
        queryset = Inventory.objects.all()

    context = {
        "object_list": queryset
    }
    return render(request, "./Inventory/inventory_list.html", context)


def inventory_detail_view(request, id):
    obj = get_object_or_404(Inventory, id=id)
    context = {
        "object": obj
    }
    return render(request, "./Inventory/inventory_detail.html", context)


def inventory_delete_view(request, id):
    obj = get_object_or_404(Inventory, id=id)
    if request.method == "POST":
        obj.delete()
        return redirect("../../")
    context = {
        "object": obj
    }
    return render(request, "./Inventory/inventory_delete.html", context)


def inventory_update_view(request, id=id):
    inventory = get_object_or_404(Inventory, id=id)
    article = inventory.article_id
    url = inventory.url_id

    article_form = ArticleForm(request.POST or None, instance=article)
    inventory_form = InventoryForm(request.POST or None, instance=inventory)
    url_form = UrlForm(request.POST or None, instance=url)
    state_form = InventoryStateDropdownForm(request.POST or None, instance=inventory.state_id,
                                            initial={'state': inventory.state_id.pk})
    category_form = CategoryDropdownForm(request.POST or None, instance=inventory.article_id.category_id,
                                         initial={'category': inventory.article_id.category_id.pk})

    if article_form.is_valid() and inventory_form.is_valid() and url_form.is_valid() \
            and state_form.is_valid() and category_form.is_valid():
        article_form.save()
        inventory_form.save()
        url_form.save()
        state_form.save()
        category_form.save()

        return redirect(inventory)

    context = {
        'article_form': article_form,
        'inventory_form': inventory_form,
        'url_form': url_form,
        'inventory_state_DropdownForm': state_form,
        'categoryDropdown_form': category_form
    }

    return render(request, "./Inventory/inventory_create.html", context)


# ENDINVENTORY

def category_create_view(request):
    form = CategoryForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/category")

    context = {
        'form': CategoryForm()
    }
    return render(request, "./Category/Category_create.html", context)


def category_list_view(request):
    queryset = Category.objects.all()
    context = {
        "object_list": queryset
    }
    return render(request, "./Category/Category_list.html", context)


def category_detail_view(request, id):
    obj = get_object_or_404(Category, id=id)
    context = {
        "object": obj
    }
    return render(request, "./Category/Category_detail.html", context)


def category_delete_view(request, id):
    obj = get_object_or_404(Category, id=id)
    if request.method == "POST":
        obj.delete()
        return redirect("../../")
    context = {
        "object": obj
    }
    return render(request, "./Category/Category_delete.html", context)


def category_update_view(request, id=id):
    obj = get_object_or_404(Category, id=id)
    form = CategoryForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
    context = {
        "form": form
    }
    return render(request, "./Category/Category_create.html", context)

def inventory_search_view(request):
    form = InventorySearchForm(request.POST or None)
    context = {
        "form": form
    }
    if request.method == "POST":
        queryset = Inventory.objects.all().filter(article_id__name__contains=form["name"].value())
        context = {
            "queryset": queryset
        }
    return render(request, "./Inventory/inventory_search.html", context)

def inventory_change_state(request, id=id):
    obj = get_object_or_404(Inventory, id=id)
    form = InventoryChangeStateForm(request.POST or None)

    if form.is_valid():
        inventoryorder = StatesInventory.objects.get(pk=form["state"].value())
        obj.state_id = inventoryorder
        obj.save()
    context = {
        "form": form
    }
    return render(request, "./Inventory/inventory_changeState.html", context)



def order_change_state(request, id=id):
    obj = get_object_or_404(Order, id=id)
    form = OrderChangeStateForm(request.POST or None)

    if form.is_valid():
        stateorder = StatesOrder.objects.get(pk=form["state"].value())
        obj.state_id = stateorder
        obj.save()
    context = {
        "form": form
    }
    return render(request, "./Order/order_changeState.html", context)

# Neuer Tag, neues Glück!

def inventory_category(request):
    form = InventoryCategoryForm(request.POST or None)

    context = {
        "form" : form
    }
    if request.method == "POST":
        cat = Category.objects.get(pk=form["name"].value()).name
        queryset = Inventory.objects.all().filter(article_id__category_id__name=cat)
        context = {
            "queryset" : queryset
        }
    return render(request, "./Inventory/inventory_category.html", context)

def user_personal_view(request):
    loggedinuser = User.objects.get(username=request.user)
    queryset = Order.objects.all().filter(owner_id=loggedinuser)
    context = {
        "queryset" : queryset
    }
    return render(request, "./User/user_personal.html", context)

def home_view(request):
    return render(request, "home.html")


def category_inventory_view(request, name):
    bfr = Category.objects.filter(name=name)
    queryset = Category.objects.none()
    if bfr.exists():
        obj = bfr[0]
    else:
        obj = None
    if obj!=None:
        queryset = Inventory.objects.filter(article_id__category_id=obj)
        context = {
            "object_list": queryset
        }
    else:
        context = {
            "object_list": queryset
        }
    return render(request, "./Inventory/inventory_category_home.html", context)

def help_view(request):
    return render(request, "./Help/help.html")

def cart_view(request):
    queryset = Order.objects.filter(owner_id=request.user)
    context = {
        "queryset" : queryset
    }
    return render(request, "./Cart/cart.html", context)

def profile_view(request):
    user = User.objects.get(username=request.user.username)
    context = {
        "queryset": user

    }
    return render(request, "./registration/profile.html", context)



# testing area...

def logout_view(request):
    logout(request)
    return render(request, "User/user_logout.html")

def ordertoInventoryAll_view(request):
    object_list = Order.objects.all()

    if request.method == "POST":
        test = request.POST
        myDict = test.dict()
        value = myDict.get("product_id", "")
        obj = get_object_or_404(Order, id=value)
        usr = obj.owner_id
        print(usr)
        inventory = Inventory.objects.create(
                                             amount=obj.amount,
                                             description=obj.description,
                                             article_id=obj.url_id.article_id,
                                             location_id=obj.owner_id.location_id_id,
                                             order_id=obj,
                                             state_id=StatesInventory.objects.first(),
                                             url_id=obj.url_id,
                                             user_id=obj.owner_id_id
                                             )
        inventory.save()
        obj.state_id = StatesOrder.objects.get(id=2)
        obj.save()
        return redirect('/orders/toInventory')
    context = {
        "object_list": object_list
    }
    return render(request, "Order/order_toInventoryAll.html", context)

def inventorychangeStateAll_view(request):
    object_list = Inventory.objects.all()
    form = InventoryChangeStateForm(request.POST or None)

    if request.method == "POST" and form.is_valid():
        state_inventory = StatesInventory.objects.get(pk=form["state"].value())
        test = request.POST
        myDict = test.dict()
        value = myDict.get("product_id", "")
        obj = get_object_or_404(Inventory, id=value)
        obj.state_id = state_inventory
        obj.save()
        return redirect('/inventory/')

    context = {
            "object_list": object_list,
            "form": form
    }
    return render(request, "Inventory/inventory_changeStateAll.html", context)
