from django import forms
from django.forms import Select
from django.contrib.auth.forms import AuthenticationForm

from .models import Order, Article, Inventory, Category, Url, StatesOrder, StatesInventory, User, Location


# order forms

# class OrderForm(forms.ModelForm):
#   name = forms.CharField(max_length=200, label='Name')
#  amount = forms.IntegerField(label='Amount',
#                             widget=forms.TextInput(attrs={"placeholder": "Your amount"}))
# delivery_date = forms.DateField(widget=forms.AdminDateWidget)
# description = forms.CharField(
#   required=False,
#  widget=forms.Textarea(
#     attrs={
#        "rows": 10,
#       "cols": 120
#  }
# )
# )

# url = forms.URLField(widget=forms.URLInput())

# class Meta:  # init von den Feldern
#   model = Order
#  fields = [
#     'name',
#    'amount',
#   'delivery_date',
#  'description',
# 'url'
# ]


# statesorder forms

class StatesOrderForm(forms.ModelForm):
    state = forms.CharField(max_length=200)

    class Meta:
        model = StatesOrder
        fields = [
            'state'
        ]


class StatesInventoryForm(forms.ModelForm):
    state = forms.CharField(max_length=200)

    class Meta:
        model = StatesInventory
        fields = [
            'state'
        ]


class UserForm(forms.ModelForm):
    username = forms.CharField(max_length=200)
    password = forms.CharField(widget=forms.PasswordInput, max_length=200)
    last_name = forms.CharField(max_length=200)
    first_name = forms.CharField(max_length=200)
    is_superuser = forms.BooleanField(required=False)
    is_secretary = forms.BooleanField(required=False)
    email = forms.EmailField(max_length=200)
    tel = forms.CharField(max_length=200)

    class Meta:
        model = User
        fields = [
            'username',
            'password',
            'last_name',
            'first_name',
            'is_superuser',
            'is_secretary',
            'email',
            'tel',
        ]


class LocationForm(forms.ModelForm):
    postcode = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={
            'class': 'form-control'
        }
    ))
    city = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={
            'class': 'form-control'
        }
    ))
    street = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={
            'class': 'form-control'
        }
    ))
    streetnumber = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={
            'class': 'form-control'
        }
    ))
    room = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={
            'class': 'form-control'
        }
    ))

    class Meta:
        model = Location
        fields = [
            'postcode',
            'city',
            'street',
            'streetnumber',
            'room',
        ]


class OrderForm(forms.ModelForm):
    amount = forms.IntegerField(max_value=100, widget=forms.NumberInput(
        attrs={
            'class': 'form-control',

        }
    ))
    delivery_date = forms.DateField(widget=forms.DateInput(
        attrs={
            'class': 'form-control',
            'type': 'date'
        }
    ))
    description = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Insert a description for this article'
        }
    ))

    class Meta:
        model = Order
        fields = [
            'amount',
            'delivery_date',
            'description',
        ]


class UrlForm(forms.ModelForm):
    url = forms.URLField(widget=forms.URLInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Enter your link for the order'
        }
    ))

    class Meta:
        model = Url
        fields = [
            'url',
        ]


class ArticleForm(forms.ModelForm):
    name = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Name of the article '
        }
    ))

    class Meta:
        model = Article
        fields = [
            'name',
        ]


class CategoryDropdownForm(forms.ModelForm):
    category = forms.ModelChoiceField(queryset=Category.objects.all().order_by('name'))

    class Meta:
        model = Category
        fields = {
            'category': Select()
        }


# Category

class CategoryForm(forms.ModelForm):
    name = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={
            'class': 'form-control'
        }
    ))

    class Meta:
        model = Category
        fields = [
            'name'
        ]


class InventoryForm(forms.ModelForm):
    amount = forms.IntegerField(max_value=100, widget=forms.NumberInput(
        attrs={
            'class': 'form-control'
        }
    ))
    description = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Insert a description for this article'
        }
    ))

    class Meta:
        model = Inventory
        fields = {
            'amount',
            'description',
        }


class InventoryStateDropdownForm(forms.ModelForm):
    state = forms.ModelChoiceField(queryset=StatesInventory.objects.all().order_by('state'))

    class Meta:
        model = StatesInventory
        fields = {
            'state': Select()
        }


class InventorySearchForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control'
        }
    ))

    class Meta:
        model = Inventory
        fields = {
            'name'
        }


class UserSelectForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=User.objects.all().order_by('username'))

    class Meta:
        model = StatesInventory
        fields = {
            'user': Select()
        }


class InventoryChangeStateForm(forms.ModelForm):
    state = forms.ModelChoiceField(queryset=StatesInventory.objects.all().order_by('state'),
                                   widget=forms.Select(attrs={"onChange": 'refresh(this)'}))

    class Meta:
        model = StatesInventory
        fields = {
            'state': Select()
        }


class LocationSelectForm(forms.ModelForm):
    location = forms.ModelChoiceField(queryset=Location.objects.all().order_by('city'))

    class Meta:
        model = Location
        fields = {
            'location': Select()
        }


class OrderChangeStateForm(forms.ModelForm):
    state = forms.ModelChoiceField(queryset=StatesOrder.objects.all().order_by('state'))

    class Meta:
        model = StatesOrder
        fields = {
            'state': Select()
        }


class InventoryCategoryForm(forms.ModelForm):
    category = forms.ModelChoiceField(queryset=Category.objects.all().order_by('name'))

    class Meta:
        model = Category
        fields = {
            'category': Select()
        }


class LoginForm(AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(
        attrs={"class": "form-control"}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={"class": "form-control"}))
