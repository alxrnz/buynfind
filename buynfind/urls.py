"""buynfind URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views

# own imports
from database import views



urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('django.contrib.auth.urls')),

    path('statesorders/create/', views.statesorder_create_view, name="StateOrders_create"),
    path('statesorders/list/', views.statesorder_list_view, name="StateOrders_list"),
    path('statesorders/', views.statesorder_list_view, name="StateOrders_list"),
    path('statesorders/<int:id>/', views.statesorder_detail_view, name="StatesOrders_detail"),
    path('statesorders/<int:id>/delete/', views.statesorder_delete_view, name="StatesOrders_delete"),
    path('statesorders/<int:id>/update/', views.statesorder_update_view, name="StatesOrders_update"),

    path('statesinventory/create/', views.statesinventory_create_view, name="StateInventory_create"),
    path('statesinventory/list/', views.statesinventory_list_view, name="StateInventory_list"),
    path('statesinventory/', views.statesinventory_list_view, name="StateInventory_list"),
    path('statesinventory/<int:id>/', views.statesinventory_detail_view, name="StatesInventory_detail"),
    path('statesinventory/<int:id>/delete/', views.statesinventory_delete_view, name="StatesInventory_delete"),
    path('statesinventory/<int:id>/update/', views.statesinventory_update_view, name="StatesInventory_update"),

    path('users/create/', views.user_create_view, name="Users_create"),
    path('users/list/', views.user_list_view, name="Users_list"),
    path('users/', views.user_list_view, name="Users_list"),
    path('users/<int:id>/', views.user_detail_view, name="Users_detail"),
    path('users/<int:id>/delete/', views.user_delete_view, name="Users_delete"),
    path('users/<int:id>/update/', views.user_update_view, name="User_update"),  # User Update not working
    path('users/style.css', views.user_list_view),

    path('orders/create/', views.order_create_view, name="database_order_create"),
    path('orders/list/', views.order_list_view, name="database_order_list"),
    path('orders/', views.order_list_view, name="database_order_list"),
    path('orders/<int:id>/', views.order_detail_view, name="database_order_detail"),
    path('orders/<int:id>/delete/', views.order_delete_view, name="database_order_delete"),
    path('orders/<int:id>/update/', views.order_update_view, name="database_order_update"),  # orders Update not working
    path('orders/<int:id>/toInventory/', views.order_toinventory_view, name="database_order_toInventory"),
    path('orders/style.css', views.order_list_view),

    path('inventory/create/', views.inventory_create_view, name="database_inventory_create"),
    path('inventory/', views.inventory_list_view, name="database_inventory_list"),
    path('inventory/<int:id>/', views.inventory_detail_view, name="database_inventory_detail"),
    path('inventory/<int:id>/delete/', views.inventory_delete_view, name="database_inventory_delete"),
    path('inventory/<int:id>/update/', views.inventory_update_view, name="database_inventory_update"),

    path('category/create/', views.category_create_view, name="Category_create"),
    path('category/list/', views.category_list_view, name="Category_list"),
    path('category/', views.category_list_view, name="Category_list"),
    path('category/<int:id>/', views.category_detail_view, name="Category_detail"),
    path('category/<int:id>/delete/', views.category_delete_view, name="Category_delete"),
    path('category/<int:id>/update/', views.category_update_view, name="Category_update"),

    path('inventory/search/', views.inventory_search_view, name="database_inventory_search"),

    path('inventory/<int:id>/changestate/', views.inventory_change_state, name="database_inventory_changeState"),
    path('orders/<int:id>/changestate/', views.order_change_state, name="database_order_changeState"),

    # Neuer Tag, neues Glück!

    path('inventory/category/', views.inventory_category, name="database_inventory_category"),
    path('personal/', views.user_personal_view, name="User_personal"),
    path('', views.home_view, name="Home"),
    path('style/', views.home_view, name="Style"),
    path('inventory/<str:name>/', views.category_inventory_view, name="Category_inventory"),
    path('help/', views.help_view, name="Help"),
    path('cart/', views.cart_view, name="Cart"),
    path('accounts/home/', views.home_view, name="Home"),
    path('accounts/profile/', views.profile_view, name="Profile"),

    path('logout/', views.logout_view, name="Logout"),
    path('orders/toInventory', views.ordertoInventoryAll_view, name="ToInventory"),
    path('inventory/changeState', views.inventorychangeStateAll_view, name="ChangeState")


]
